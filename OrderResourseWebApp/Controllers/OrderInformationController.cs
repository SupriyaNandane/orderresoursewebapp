﻿using Newtonsoft.Json;
using OrderResourseWebApp.Helper;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using XMLFileLibrary;

namespace OrderResourseWebApp.Controllers
{
    public class OrderInformationController : ApiController
    {
        ConvertTextToXMLFile convertTextToXML;
        string path = string.Empty;
        string xmlInputData = string.Empty;
        private Serializer ser;

        public OrderInformationController()
        {
            convertTextToXML = new ConvertTextToXMLFile();
            ser = new Serializer();
            path = @"D:\\OrderXMLFile.xml";
            xmlInputData = File.ReadAllText(path);
        }

        // GET api/OrderInformation
        public string Get()
        {
            string xmlOutputData = string.Empty;
            List<OrderInformationModel> orderInformationModel = ser.Deserialize<List<OrderInformationModel>>(xmlInputData);
            xmlOutputData = ser.Serialize(orderInformationModel);
            return JsonConvert.SerializeObject(xmlOutputData);
        }

        // GET api/OrderInformation/5
        public string Get(int id)
        {
            List<OrderInformationModel> orderInformationModel = ser.Deserialize<List<OrderInformationModel>>(xmlInputData);
            var orderInformationModelList = new List<OrderInformationModel>();

            foreach (var item in orderInformationModel)
            {

                if (item.OrderNumber == id)
                {
                    orderInformationModelList.Add(item);
                }
                else
                {
                    continue;
                }

            }

            if (orderInformationModelList.Any())
                return JsonConvert.SerializeObject(orderInformationModelList);
            else
                return JsonConvert.SerializeObject("No details found");
        }
    }
}
