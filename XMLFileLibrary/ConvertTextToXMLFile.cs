﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace XMLFileLibrary
{
    public class ConvertTextToXMLFile
    {
        public List<OrderInformationModel> mainList = new List<OrderInformationModel>();
        public ConvertTextToXMLFile()
        {

            CreateOrderDetailsList("order1.txt");
            CreateOrderDetailsList("order2.txt");
            CreateOrderDetailsList("order3.txt");

            var xmlFileName = "D:\\OrderXMLFile.xml";

            var path = new FileStream(xmlFileName, FileMode.Create);

            using (FileStream fs = path)
            {
                new XmlSerializer(typeof(List<OrderInformationModel>)).Serialize(fs, mainList);
            }
        }

        private void CreateOrderDetailsList(string fileName)
        {

            string[] orderFile = File.ReadAllLines("C:\\Users\\Supriya\\source\\repos\\OrderResourceApplication\\Order files\\" + fileName);
            List<OrderInformationModel> list = new List<OrderInformationModel>();

            for (var i = 1; i < orderFile.Length; i++)
            {
                string[] content = orderFile[i].Split('|');
                OrderInformationModel orderInformationModel = new OrderInformationModel
                {
                    OrderNumber = Convert.ToInt32(content[0 + 1]),
                    OrderLineNumber = content[2],
                    ProductNumber = content[3],
                    Quantity = Convert.ToInt32(content[4]),
                    Name = content[5],
                    Description = content[6],
                    Price = Convert.ToDouble(content[7]),
                    ProductGroup = content[8],
                    OrderDate = content[9],
                    CustomerName = content[10],
                    CustomerNumber = Convert.ToInt32(content[11])

                };
                list.Add(orderInformationModel);
            }
            mainList.AddRange(list);
        }
    }
}
